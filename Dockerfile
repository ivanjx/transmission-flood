# Downloading flood UI.
FROM ubuntu:focal as flood
RUN apt-get update && \
    apt-get install curl -y
RUN curl -OL https://github.com/johman10/flood-for-transmission/releases/download/2022-08-29T06-44-30/flood-for-transmission.tar.gz
RUN tar -xvf flood-for-transmission.tar.gz

# Transmission runtime.
FROM linuxserver/transmission:arm64v8-3.00-r5-ls135
COPY --from=flood /flood-for-transmission /flood-for-transmission
RUN rm /flood-for-transmission/sw.js
ENV TRANSMISSION_WEB_HOME=/flood-for-transmission/
